const express = require('express')

const auth = require('../middlewares/auth')
const conversionController = require('../controller/conversion.controller');
const { check } = require('express-validator')

const router = express.Router();
router.post('/doc-pdf',auth,conversionController.convertDoctoPdf);
router.get('/get-token',conversionController.getToken);  
module.exports = router;

