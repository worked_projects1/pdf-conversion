const express = require('express')
const auth = require('../middlewares/auth')
const passwordRemoveController = require('../controller/removePassword.controller');
const { check } = require('express-validator')

const router = express.Router();
router.post('/extract-data',passwordRemoveController.RemovePassword);
router.post('/check-file-status',passwordRemoveController.checkEncryptionStatus);
module.exports = router;