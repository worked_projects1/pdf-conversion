require('dotenv').config();
const path = require('path');
const fs = require('fs');
const exec = require('child_process').execSync;
const AWS = require('aws-sdk');
const s3 = new AWS.S3({ region: 'us-east-1' });
const { createToken } = require('../utils/authenticate');
exports.convertDoctoPdf = async (req,res) => {
    try {
    console.log(req.body);
    const { s3_key, env} = await req.body;
    if(!s3_key) return res.status(500).json({status:'failure',message:'s3 file key missing'});
    if(!env) return res.status(500).json({status:'failure',message:'environment value missing'});
    
    let s3DirPath = s3_key.split('/');
    let case_id = s3DirPath[0];
    
    const bucketname = env == 'staging'?process.env.S3_BUCKET_FOR_FORMS_STAGING : process.env.S3_BUCKET_FOR_FORMS_PRODUCTION;
    let docFilename = s3_key.split('/').pop();
    let pdfFileName;
    
    if(env == 'production' || env == 'staging') {
        const docName = docFilename.split('.docx').join('').split('_');
        pdfFileName = `${docName[0]}.pdf`;
    }

    let s3Dir = await s3DirPath.splice(0, s3DirPath.length-1).join('/');
    console.log('s3 Directory : '+s3Dir);
    const downloadPath = path.join(__dirname, `../resources/${case_id}/`);

    if (!fs.existsSync(downloadPath)){
        fs.mkdirSync(downloadPath);
    }else{
        console.log('create new Directory');
    }
    

    let bucketUrl = `s3://${bucketname}/${s3_key}`;
    console.log('downloadPath :'+downloadPath);
    console.log('Bucket URL :'+bucketUrl);
    let downloadUrl = `aws s3 cp "${bucketUrl}" ${downloadPath}`;
    console.log('Download URL :'+downloadUrl)
    console.log('-----'+docFilename);    
    let converstion = `soffice --headless --convert-to pdf ${downloadPath}"${docFilename}" --outdir ${downloadPath}`;
    let fileuploadUrl = `aws s3 cp ${downloadPath}"${pdfFileName}" s3://${bucketname}/${s3Dir}/"${pdfFileName}"`;
    console.log(fileuploadUrl);
    let removeFileDir = `rm -r resources/${case_id}`;
    const fileDownload = exec(downloadUrl).toString(); 
    const fileConverstion = exec(converstion).toString(); 
    const fileupload = exec(fileuploadUrl).toString(); 
    const removeFile = exec(removeFileDir).toString(); 

     console.log(' fileDownload \n :'+fileDownload);
     console.log('fileConverstion \n :' +fileConverstion);
     console.log('fileuploadF \n : '+ fileupload);
     console.log('removeFile \n : '+ removeFile);

     return res.status(200)
        .json({
        status:'success',
        message:'pdf file uploaded successfully', 
        pdf_s3_key:`${s3Dir}/${pdfFileName}`
    });
    } catch (err) {
        console.log(err);
        return await res.status(500).json({status:'failure',message:err.message});   
    }
    
}
exports.getToken = (req,res) => {
    try {
        const token = createToken();
        console.log(token);
        return res.status(200).json({status:'success',key:token});
    } catch (err) {
        console.log(err);
        return res.status(500).json({status:'failure',message:err.message});
    }
}

/* 
==================Its just a waring not an error.=================================
    Just tried to launch Writer from console to see if it spits any error messages.
And it does before it hangs and latter dies.

<filelocation> : $ soffice <filename>.<doctype>
E: lt_string_value: assertion `string != ((void *)0)' failed
E: lt_string_value: assertion `string != ((void *)0)' failed
E: lt_string_value: assertion `string != ((void *)0)' failed
E: lt_string_value: assertion `string != ((void *)0)' failed

*/