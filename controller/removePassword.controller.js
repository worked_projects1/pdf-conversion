require('dotenv').config();
const path = require('path');
const fs = require('fs');
const exec = require('child_process').execSync;
const { createToken } = require('../utils/authenticate');
const uuid = require("uuid");

const RemovePassword = (req, res) => {
    try {
        const { s3_key, env, password } =req.body;
        if(!s3_key) return res.status(500).json({status:'failure',message:'s3 file key missing.'});
        // if(!env) return res.status(500).json({status:'failure',message:'environment value missing.'});
        if(!password) return res.status(500).json({status:'failure',message:'Password not found.'});
        const bucketname = env == 'staging'?process.env.S3_BUCKET_FOR_PRIVATE_ASSETS_STAGING : process.env.S3_BUCKET_FOR_PRIVATE_ASSETS;
        let docFilename = s3_key.split('/').pop();
        
        let pdfFileName = docFilename.split('.').slice(0, -1);
        pdfFileName.push('pdf');
        pdfFileName = pdfFileName.join('.');
    
        let s3Dir = s3_key.split('/');
        const random_id = uuid.v4(); // practice ID
        // let random_id = '10002';
        s3Dir = s3Dir.splice(0, s3Dir.length-1).join('/');

        console.log('s3 Directory : '+s3Dir);
        const downloadPath = path.join(__dirname, `../resources/removePasswords/${random_id}`);
        const outputPath = path.join(__dirname, `../resources/removePasswords/${random_id}/output`);
    
        if (!fs.existsSync(downloadPath)){
            fs.mkdirSync(downloadPath);
        }else{
            console.log('create new Directory');
        }
        if (!fs.existsSync(outputPath)){
            fs.mkdirSync(outputPath);
        }

        console.log(' outputPath : '+outputPath);
        console.log(' downloadPath : '+downloadPath);

        let bucketUrl = `s3://${bucketname}/${s3_key}`;
        console.log('downloadPath :'+downloadPath);
        console.log('Bucket URL :'+bucketUrl);
        let downloadUrl = `aws s3 cp "${bucketUrl}" "${downloadPath}/${random_id}.pdf"`;
        let converstion = `soffice --headless --convert-to pdf "${downloadPath}/${docFilename}" --outdir ${downloadPath}`;

        let decryptioncmd = `qpdf --password=${password} --decrypt "${downloadPath}/${random_id}.pdf" "${outputPath}/${random_id}.pdf" --warning-exit-0`;
        let fileuploadUrl = `aws s3 cp "${outputPath}/${random_id}.pdf" "s3://${bucketname}/${s3Dir}/${docFilename}"`;
        let removeFileDir = `rm -r resources/removePasswords/${random_id}`;
        console.log('Download URL :'+downloadUrl)
        console.log('-----'+docFilename);    
        console.log('decryptioncmd : '+decryptioncmd);    
        const fileDownload = exec(downloadUrl).toString();
        const decryptFile = exec(decryptioncmd).toString();
        const fileUpload = exec(fileuploadUrl).toString();
        const removeFile = exec(removeFileDir).toString(); 

        console.log(' fileDownload \n :'+fileDownload);
        console.log('decryptFile \n :' +decryptFile);
        console.log('fileUpload \n : '+ fileUpload);
        console.log('removeFile \n : '+ removeFile);
        
        res.status(200).json({ status: 'Success',s3_key: `${s3Dir}/${docFilename}`});

    } catch (err) {
        console.log(err);
        res.status(500).json({ status: 'failure', message: err.message });
    }
}
const checkEncryptionStatus = (req, res) => {
    try {
        const { s3_key, env, password } =req.body;
        if(!s3_key) return res.status(500).json({status:'failure',message:'s3 file key missing.'});
        if(!password) return res.status(500).json({status:'failure',message:'Password not found.'});
        const bucketname = env == 'staging'? process.env.S3_BUCKET_FOR_PRIVATE_ASSETS_STAGING : process.env.S3_BUCKET_FOR_PRIVATE_ASSETS;
        let docFilename = s3_key.split('/').pop();


        let pdfFileName = docFilename.split('.').slice(0, -1);
        pdfFileName.push('pdf');
        pdfFileName = pdfFileName.join('.');
    
        let s3Dir = s3_key.split('/');
        const random_id = uuid.v4(); 
        s3Dir = s3Dir.splice(0, s3Dir.length-1).join('/');

        console.log('s3 Directory : '+s3Dir);
        const downloadPath = path.join(__dirname, `../resources/removePasswords/${random_id}`);
        const outputPath = path.join(__dirname, `../resources/removePasswords/${random_id}/output`);
    
        if (!fs.existsSync(downloadPath)){
            fs.mkdirSync(downloadPath);
        }else{
            console.log('create new Directory');
        }
        if (!fs.existsSync(outputPath)){
            fs.mkdirSync(outputPath);
        }
        const bucketUrl = `s3://${bucketname}/${s3_key}`;
        const downloadUrl = `aws s3 cp "${bucketUrl}" ${downloadPath}/${random_id}.pdf`;
        console.log(downloadUrl);
        const noOfPagesCMD = `qpdf --show-npages "${downloadPath}/${random_id}.pdf" --password=${password} --warning-exit-0`;
        console.log(' noOfPagesCMD : '+noOfPagesCMD);
        const checkEncryptionCMD = `qpdf "${downloadPath}/${random_id}.pdf" --show-encryption --warning-exit-0`;
        console.log(' checkEncryptionCMD : '+checkEncryptionCMD);
        const removeFileDir = `rm -r resources/removePasswords/${random_id}`;
    
        const fileDownload = exec(downloadUrl).toString();
        const noOfPages = exec(noOfPagesCMD).toString();
        let encryptionStatus = false;
        try {
            encryptionStatus = exec(checkEncryptionCMD).toString();
            encryptionStatus = encryptionStatus.trim();
            if(encryptionStatus == 'File is not encrypted') encryptionStatus = false;
        } catch (error) {
            encryptionStatus = true;
        }
        const removeFile = exec(removeFileDir).toString(); 
        res.status(200).json({no_of_pages: parseInt(noOfPages) , encryption_status: encryptionStatus});
    } catch (err) {
        console.log(err);
        res.status(500).json({ status: 'failure', message: err.message });
    }
}

module.exports = {
    RemovePassword,
    checkEncryptionStatus
}