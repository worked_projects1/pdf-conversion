require('dotenv').config();
const CryptoJS = require("crypto-js");
const HTTPError = require('../middlewares/error');

const data = process.env.ENCRYPTION_KEY;
const privateKey = process.env.PRIVATE_KEY;
const publicKey = process.env.PUBLIC_KEY;

exports.createToken = () => {
    try {
        const ciphertext = CryptoJS.AES.encrypt(privateKey, publicKey).toString();
        return ciphertext;
    } catch (err) {
            console.log(err);
            return err;
    }
}

exports.verifyToken = (encryptedData) => {
    try {
        console.log('inside verification');
        var bytes = CryptoJS.AES.decrypt(encryptedData, publicKey);
        var secret = bytes.toString(CryptoJS.enc.Utf8);
        secret = JSON.parse(secret);
        if(secret.key == privateKey) return secret;
        else return false;
    } catch (err) {
        console.log(err);
        return err;
    }
}

/* 

    var bytes = CryptoJS.AES.decrypt(result.authToken, record.id);
    var secret = bytes.toString(CryptoJS.enc.Utf8);

*/