const express = require("express");
const app = express();
const cors = require("cors");
app.use(express.json());
app.use(cors());
app.options('*', cors());
app.use(express.urlencoded({ extended: true }));

const { errorMiddleware } = require('./middlewares/error');
const conversion = require('./routes/conversion.routes');
const removePasswords = require('./routes/removePassword.routes');
app.get('/' , (req, res) => {
    res.status(200).send('Api working')
  })
console.log('test');
app.use('/conversion', conversion);  
app.use('/protected-pdf', removePasswords);  
app.get('/' , (req, res) => {
    res.status(200).send('Api working')
  })
  

app.use(errorMiddleware)

const PORT = process.env.PORT || 3000

app.listen(PORT, (err) => {
    if(err) console.log(err);
    console.log(`Server started on port ${PORT}`)
});